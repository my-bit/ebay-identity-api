#!/bin/bash

openapi-generator generate -i commerce_identity_v1_oas3.yaml -g php -o . --additional-properties=apiPackage=Api,invokerPackage=MyBit\\Ebay\\Identity,artifactVersion=1.1.0,composerPackageName=my-bit/ebay-identity-api