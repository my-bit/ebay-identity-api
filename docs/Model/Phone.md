# # Phone

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country_code** | **string** | The two-letter &lt;a href&#x3D;\&quot;https://www.iso.org/iso-3166-country-codes.html\&quot;&gt;ISO 3166&lt;/a&gt; standard of the country to which the phone number belongs. | [optional]
**number** | **string** | The numeric string representing the phone number. | [optional]
**phone_type** | **string** | The type of phone service. &lt;br /&gt;&lt;br /&gt;&lt;b&gt; Valid Values: &lt;/b&gt; MOBILE or LAND_LINE  &lt;br /&gt;&lt;br /&gt;Code so that your app gracefully handles any future changes to this list. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
