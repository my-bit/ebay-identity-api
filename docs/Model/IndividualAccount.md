# # IndividualAccount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** | The eBay user&#39;s registration email address. | [optional]
**first_name** | **string** | The eBay user&#39;s first name. | [optional]
**last_name** | **string** | The eBay user&#39;s last name. | [optional]
**primary_phone** | [**\MyBit\Ebay\Identity\Model\Phone**](Phone.md) |  | [optional]
**registration_address** | [**\MyBit\Ebay\Identity\Model\Address**](Address.md) |  | [optional]
**secondary_phone** | [**\MyBit\Ebay\Identity\Model\Phone**](Phone.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
