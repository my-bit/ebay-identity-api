# # UserResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_type** | **string** | Indicates the user account type. This is determined when the user registers with eBay. If they register for a business account, this value will be BUSINESS. If they register for a private account, this value will be INDIVIDUAL. This designation is required by the tax laws in the following countries:   &lt;br /&gt;&lt;br /&gt;EBAY_AT, EBAY_BE, EBAY_CH, EBAY_DE, EBAY_ES, EBAY_FR, EBAY_GB, EBAY_IE, EBAY_IT, EBAY_PL &lt;br /&gt;&lt;br /&gt;&lt;b&gt; Valid Values:&lt;/b&gt; BUSINESS or INDIVIDUAL &lt;br /&gt;&lt;br /&gt;Code so that your app gracefully handles any future changes to this list. For implementation help, refer to &lt;a href&#x3D;&#39;https://developer.ebay.com/api-docs/commerce/identity/types/api:AccountTypeEnum&#39;&gt;eBay API documentation&lt;/a&gt; | [optional]
**business_account** | [**\MyBit\Ebay\Identity\Model\BusinessAccount**](BusinessAccount.md) |  | [optional]
**individual_account** | [**\MyBit\Ebay\Identity\Model\IndividualAccount**](IndividualAccount.md) |  | [optional]
**registration_marketplace_id** | **string** | The eBay site on which the account is registered. For implementation help, refer to &lt;a href&#x3D;&#39;https://developer.ebay.com/api-docs/commerce/identity/types/bas:MarketplaceIdEnum&#39;&gt;eBay API documentation&lt;/a&gt; | [optional]
**status** | **string** | Indicates the user&#39;s account status. Possible values: &lt;code&gt;CONFIRMED&lt;/code&gt;, &lt;code&gt;UNCONFIRMED&lt;/code&gt;, &lt;code&gt;ACCOUNTONHOLD&lt;/code&gt; and &lt;code&gt;UNDETERMINED&lt;/code&gt;. For implementation help, refer to &lt;a href&#x3D;&#39;https://developer.ebay.com/api-docs/commerce/identity/types/api:UserStatusEnum&#39;&gt;eBay API documentation&lt;/a&gt; | [optional]
**user_id** | **string** | The eBay immutable user ID of the user&#39;s account and can always be used to identify the user. | [optional]
**username** | **string** | The user name, which was specific by the user when they created the account. &lt;br /&gt;&lt;br /&gt;&lt;span class&#x3D;\&quot;tablenote\&quot;&gt;&lt;b&gt;Note: &lt;/b&gt; This value can be changed by the user.&lt;/span&gt; | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
