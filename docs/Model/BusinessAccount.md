# # BusinessAccount

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**\MyBit\Ebay\Identity\Model\Address**](Address.md) |  | [optional]
**doing_business_as** | **string** | An additional name that is used for their business on eBay. The business name is returned in the &lt;b&gt; name&lt;/b&gt; field. | [optional]
**email** | **string** | The email address of the business account. | [optional]
**name** | **string** | The business name associated with the user&#39;s eBay account. | [optional]
**primary_contact** | [**\MyBit\Ebay\Identity\Model\Contact**](Contact.md) |  | [optional]
**primary_phone** | [**\MyBit\Ebay\Identity\Model\Phone**](Phone.md) |  | [optional]
**secondary_phone** | [**\MyBit\Ebay\Identity\Model\Phone**](Phone.md) |  | [optional]
**website** | **string** | The business website address associated with the eBay account. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
